using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject play;

    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = Transform.position - player.transform.position; 
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = player.transform.positions + offset; 
    }
}
